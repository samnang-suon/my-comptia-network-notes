# My Comptia Network+ Notes
## OSI Seven-layered Model
![OSI_Model](images/OSI_Model.jpg)

## TCP/IP Model
![TCPIP_Model](images/TCPIP_Model.png)

## OSI vs TCP/IP
![OSI_TCPIP_Mapping](images/OSI_TCPIP_Mapping.png)

## Processing Data Unit (PDU)
![ProcessingDataUnitByLayer](images/ProcessingDataUnitByLayer.jpg)

## Network Cheat Sheet
![NetworkCheatSheet](images/NetworkCheatSheet.png)

## Protocols by Layer

![InternetProtocolSuite](images/InternetProtocolSuite.png)

| TCP/IP Application Protocol | Meaning |
|-----------------------------|---------|
BGP | Border Gateway Protocol
DHCP | Dynamic Host Configuration Protocol
DNS | Domain Name System **! Remember the phonebook analogy !**
FTP | File Transfer Protocol
HTTP | Hypertext Transfer Protocol
IMAP | Internet Message Access Protocol
LDAP | Lightweight Directory Access Protocol
OSPF | Open Shortest Path First
RIP | Routing Information Protocol
SIP | Session Initiation Protocol
SMTP | Simple Mail Transfer Protocol
SSH | Secure Shell Protocol

source:
* https://en.wikipedia.org/wiki/Internet_protocol_suite

## Networking Utility
| Command | Description |
|---------|-------------|
arp | When computer A have computer B's IP Address but also want B's MAC Address, computer A need to send an ARP to computer B.
getmac |
hostname |
ipconfig |
netstat |
nslookup |
pathping |
ping |
SystemInfo |
tracert |

sources:
* https://en.wikipedia.org/wiki/Network_Utility
* https://www.techrepublic.com/article/ten-windows-10-network-commands-everyone-one-should-know/

## Well-Known Application Ports

| Port | Protocol/Service |
|------|------------------|
20 | File Transfer Protocol (FTP) data transfer
21 | File Transfer Protocol (FTP) control (command)
22 | Secure Shell (SSH); secure logins, file transfers (scp, sftp) and port forwarding
23 | Telnet
25 | Simple Mail Transfer Protocol (SMTP), used for email routing between mail servers
53 | Domain Name System (DNS)
80 | Hypertext Transfer Protocol (HTTP) uses TCP in versions 1.x and 2. HTTP/3 uses QUIC, a transport protocol on top of UDP.
110 | Post Office Protocol, version 3 (POP3)
115 | Simple File Transfer Protocol (SFTP)
989 | FTPS Protocol (data), FTP over TLS/SSL
990 | FTPS Protocol (control), FTP over TLS/SSL

sources:
* https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers

## Network Topology Types
![NetworkTopology](images/NetworkTopology.png)

sources:
* https://www.dnsstuff.com/what-is-network-topology
* https://en.wikipedia.org/wiki/Network_topology

## Ethernet Cable Versions
![EthernetCablePerformanceSummary](images/EthernetCablePerformanceSummary.png)

sources:
* https://www.electronics-notes.com/articles/connectivity/ethernet-ieee-802-3/how-to-buy-best-ethernet-cables-cat-5-6-7.php

## Router vs Modem
![HomeModem](images/HomeModem.png)

sources:
* https://www.homenethowto.com/basics/introduction-home-networks/

## IP Address
### IPv4
![IPv4](images/IPv4.png)
#### Classful IPv4 Address
![ClassfulIPAddress](images/ClassfulIPAddress.png)
#### Classless Inter-Domain Routing (CIDR)
sources:
* https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing
#### Bit vs Byte vs Octet
![Bit_Byte_Octet](images/Bit_Byte_Octet.jpg)
#### How to read Binary
![HowToReadBinary](images/HowToReadBinary.png)
### Subnet Mask
sources:
* https://avinetworks.com/glossary/subnet-mask/
### IPv6
![IPv6](images/IPv6.png)

Sources:
* https://en.wikipedia.org/wiki/IP_address

## Network Address Translation (NAT)
sources:
* https://en.wikipedia.org/wiki/Network_address_translation

## Dynamic Routing Protocols: Distance Vector and Link State Protocols
Read: https://www.pluralsight.com/blog/it-ops/dynamic-routing-protocol

## URL vs URI vs URN
![URI_URN_URL](images/URI_URN_URL.png)

Read: https://danielmiessler.com/study/difference-between-uri-url/
### URL's Components
![URLComponents](images/URLComponents.png)

![PartOfURL](images/PartOfURL.png)
### Top-Level Domain Examples
![TLDExamples](images/TLDExamples.png)
### FQDN = Hostname + Domain Name
![FQDNExample](images/FQDNExample.jpg)

## CIA Triad
![CIA_Triad](images/CIA_Triad.png)

A set of principles to correctly add security feature to FTP/UDP.
* Confidentiality ---> By using encryption
* Integrity ---> By using certificate
* Availability ---> Trying to balance Confidentiality-Integrity

## Authentication vs Authorization
![AuthenticationVsAuthorization](images/AuthenticationVsAuthorization.png)

Read: https://www.okta.com/identity-101/authentication-vs-authorization
### Multi-Factor Authentication (MFA)
![MFA](images/MFA.png)

## Encryption
![EncryptionOverview](images/EncryptionOverview.png)

Read: https://www.proofpoint.com/us/threat-reference/encryption
### Classification of Encryption Algorithm
![ClassificationOfEncryptionAlgorithm](images/ClassificationOfEncryptionAlgorithm.jpeg)

## Access Control List Types
![AccessControlList](images/AccessControlList.png)

## OAuth vs OpenID
![OpenID_OAuth](images/OpenID_OAuth.png)

## Certificate Authority (CA)
![CertificateAuthority](images/CertificateAuthority.png)

## Virtual Local Area Network (VLAN)
Watch: https://www.youtube.com/watch?v=jC6MJTh9fRE

## Firewall
Watch: https://www.youtube.com/watch?v=kDEX1HXybrU

## IDS vs IPS ; Intrusion Detection Systems vs Intrusion Prevention Systems 
![IDS_IPS](images/IDS_IPS.png)

## Proxy Server
Watch: https://www.youtube.com/watch?v=5cPIukqXe5w
### Forward Proxy
![ForwardProxy](images/ForwardProxy.png)

Tips: Forward hides the client.
### Reverse Proxy
![ReverseProxy](images/ReverseProxy.png)

Tips: Reverse hides the server.
### Summary
![FORWARD_VS_REVERSE-PROXY](images/FORWARD_VS_REVERSE-PROXY.jpg)

## NAS vs SAN ; Network Attached Storage vs Storage Area Network
Watch: https://www.youtube.com/watch?v=3yZDDr0JKVc
### Summary
![SAN_VS_NAS](images/SAN_VS_NAS.jpg)

## LAN vs MAN vs WAN
![LAN_VS_MAN_VS_WAN](images/LAN_VS_MAN_VS_WAN.png)

## Unified Communication and Collaboration (UCC)
![UCC](images/UCC.jpg)

## DMZ
Watch: https://www.youtube.com/watch?v=dqlzQXo1wqo

## Security information and event management (SIEM)
Read: https://www.imperva.com/learn/application-security/siem

==============================
## Institute of Electrical and Electronics Engineers (IEEE)
Official Website: https://www.ieee.org/
### IEEE 802.3 - Standard for Ethernet
Official Website: https://standards.ieee.org/standard/802_3-2018.html
## The Internet Corporation for Assigned Names and Numbers (ICANN)
Official Website: https://www.icann.org/